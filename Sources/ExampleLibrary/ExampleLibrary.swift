public struct ExampleLibrary {
    var text = "Hello, World!"
    
    public init() {}
    
    public func greetings() -> String {
        return self.text
    }
    
    public func getVersion() -> String {
        return "1.0.0"
    }
}

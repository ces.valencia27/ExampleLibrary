import XCTest

import ExampleLibraryTests

var tests = [XCTestCaseEntry]()
tests += ExampleLibraryTests.allTests()
XCTMain(tests)
